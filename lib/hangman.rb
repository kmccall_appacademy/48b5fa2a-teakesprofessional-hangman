require 'byebug'
class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(options)
    @guesser = options[:guesser]
    @referee = options[:referee]
  end

  def play
    setup
    take_turn until over?
    conclude
  end

  def setup
    word_length = @referee.pick_secret_word
    @guesser.register_secret_length(word_length)
    @board = ''
     word_length.times do
      @board += '_'
    end
  end

  def take_turn
    guess = @guesser.guess
    correct = @referee.check_guess(guess)
    update_board(correct)
    @guesser.handle_response(correct)
  end

  def update_board(correct)
    arr = []
    update = @board.chars.each_index do |idx|
      @board.chars[idx] = @guesser.guess if correct.include?(@guesser.guess)
    end
    @board = update.join

  end

  def over?
    @board.count(nil) == 0
  end
end

class HumanPlayer

  def initialize

  end

  def register_secret_length(length)

  end

  def guess(board)
    print_board(board)
    print "Guess NOW PLZ"
    gets.chomp
  end

  def handle_response

  end

  def print_board(board)
    p board
  end
end

class ComputerPlayer
  attr_accessor :word, :candidate_words

  def self.read_dictionary
    File.readlines('dictionary.txt')
  end

  def initialize(dictionary)
    @dictionary = dictionary
    @alphabet = ("a".."z").to_a
  end

  def pick_secret_word
    @word = @dictionary.sample
    @word.length
  end

  def check_guess(lett)
    arr = []
    @word.chars.each_index do |idx|
      arr << idx if @word.chars[idx] ==  lett
    end
    arr
  end

  def guess(board)
    letter_count = Hash.new(0)
    @candidate_words
    @candidate_words.join.chars.each do |word|
      letter_count[word] += 1
    end
    # byebug
    # return key unless board.include?(key)
    loop do
      max = letter_count.values.max
      key = letter_count.key(max)
      if !board.include?(key)
        return key
      else
        letter_count.delete(key)
      end
    end
  end

  def handle_response(guess, arr)
      @candidate_words.reject! do |word|
        reject = false
        word.chars.each_with_index do |letter, index|
          if letter == guess && !arr.include?(index)
            reject = true
          elsif letter != guess && arr.include?(index)
            reject = true
          end
        end
        reject
      end
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.reject {|word| word.length != length}
  end


end

if __FILE__ == $PROGRAM_

    guesser = HumanPlayer.new
    referee = ComputerPlayer.new(dictionary)

  game = Hangman.new(guesser: guesser, referee: referee)
  game.play
end
